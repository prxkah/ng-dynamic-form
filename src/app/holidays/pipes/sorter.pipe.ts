import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'sortBy' })
export class SortByPipe implements PipeTransform {
  transform(items: any[], sortProp: string): any {
    return items.sort((a, b) => {
      return a.value[sortProp] - b.value[sortProp];
    })
  }
}
