import { Component, Input, OnInit, Self } from '@angular/core';
import { ControlValueAccessor, FormControl, Validators, NgControl } from '@angular/forms';

@Component({
  selector: 'app-holiday-field-group',
  templateUrl: './holiday-field-group.component.html'
})
export class HolidayFieldGroupComponent implements OnInit, ControlValueAccessor {
  @Input() title: string;

  touched: () => void;

  control = new FormControl('', { updateOn: 'blur' });

  constructor(@Self() public ngControl: NgControl) {
    ngControl.valueAccessor = this;
  }

  ngOnInit() {
    // const ngControl = this.ngControl.control;
    // const validators = ngControl.validator ?
    //   [ngControl.validator, Validators.required] :
    //   [Validators.required];

    // ngControl.setValidators(validators);
    // ngControl.updateValueAndValidity();
  }

  writeValue(value: string) {
    this.control.setValue(value, { emitEvent: false });
  }

  registerOnChange(fn: any) {
    this.control.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: any) {
    this.touched = fn;
  }

  setDisabledState?(isDisabled: boolean) {
    isDisabled ? this.control.disable() : this.control.enable();
  }
}
