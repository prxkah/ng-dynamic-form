import { FormGroup, FormArray } from '@angular/forms';
import { isBefore, isAfter, isSameDay } from 'date-fns';

const isSameOrAfter = (dateLeft: string, dateRight: string) =>
  isSameDay(dateLeft, dateRight) || isAfter(dateLeft, dateRight);

const isSameOrBefore = (dateLeft: string, dateRight: string) =>
  isSameDay(dateLeft, dateRight) || isBefore(dateLeft, dateRight);

export const otherHolidaysDuringSchoolBreak = (control: FormGroup) => {
  debugger;
  const otherHolidays = (control.get('otherHolidays') as FormArray).controls.map(x => x.value);
  const { value } = control;

  const invalid = otherHolidays.some(
    date =>
      (isSameOrAfter(date, value.sportBreakStart) && isSameOrBefore(date, value.sportBreakEnd)) ||
      (isSameOrAfter(date, value.easterBreakStart) && isSameOrBefore(date, value.easterBreakEnd))
  );

  return invalid ? { otherHolidaysDuringSchoolBreak: true } : null;
};
