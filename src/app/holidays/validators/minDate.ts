import { AbstractControl } from '@angular/forms';
import { isBefore } from 'date-fns';
import { setWarning } from '../helpers/setWarning';

export const minDate = (date: string) => {
  return (control: AbstractControl) => {
    return isBefore(control.value, date) ? { minDate: date } : null;
  };
};

export const minDateWarning = (date: string) => {
  return (control: AbstractControl) => {
    const warning = minDate(date)(control);
    setWarning(control, warning, 'minDateWarning');
    return null;
  };
};
