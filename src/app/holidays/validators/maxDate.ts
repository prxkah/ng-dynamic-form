import { AbstractControl } from '@angular/forms';
import { isAfter } from 'date-fns';
import { setWarning } from '../helpers/setWarning';

export const maxDate = (date: string) => {
  return (control: AbstractControl) => {
    return isAfter(control.value, date) ? { maxDate: date } : null;
  };
};

export const maxDateWarning = (date: string) => {
  return (control: AbstractControl) => {
    const warning = maxDate(date)(control);
    setWarning(control, warning, 'maxDateWarning');
    return null;
  };
};
