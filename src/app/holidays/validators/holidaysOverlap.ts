import { FormGroup } from '@angular/forms';
import { isAfter } from 'date-fns';

const getOverlappingDates = (dates: string[]) => {
  const overlappingDates = [];
  for (let i = 0; i < dates.length - 1; i++) {
    const curr = dates[i];
    const next = dates[i + 1];

    if (isAfter(curr, next)) {
      overlappingDates.push([curr, next]);
    }
  }
  return overlappingDates;
};

export const holidaysOverlap = (control: FormGroup) => {
  const holidays = [
    control.value.workyearStart,
    control.value.autumnSeasonEnd,
    control.value.springSeasonStart,
    control.value.sportBreakStart,
    control.value.sportBreakEnd,
    control.value.easterBreakStart,
    control.value.easterBreakEnd,
    control.value.workyearEnd
  ];

  const invalidDates = getOverlappingDates(holidays);

  return invalidDates.length > 0 ? { dateOverlap: invalidDates } : null;
};
