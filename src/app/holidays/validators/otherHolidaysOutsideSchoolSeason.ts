import { FormGroup, FormArray } from '@angular/forms';
import { isBefore, isAfter } from 'date-fns';

export const otherHolidaysOutsideSchoolSeason = (control: FormGroup) => {
  const otherHolidays = (control.get('otherHolidays') as FormArray).controls.map(x => x.value);
  const { value } = control;

  debugger;

  const invalid = otherHolidays.some(date =>
    (isBefore(date, value.workyearStart) || isAfter(date, value.autumnSeasonEnd)) &&
    (isBefore(date, value.springSeasonStart) || isAfter(date, value.workyearEnd))
  );

  return invalid ? { otherHolidaysOutsideSchoolSeason: true } : null;
};
