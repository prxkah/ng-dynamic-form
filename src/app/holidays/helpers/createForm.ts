import { Validators, FormControl, FormArray, FormGroup, ValidatorFn } from '@angular/forms';
import { minDate, minDateWarning } from '../validators/minDate';
import { maxDate, maxDateWarning } from '../validators/maxDate';
import { holidaysOverlap } from '../validators/holidaysOverlap';
import { otherHolidaysDuringSchoolBreak } from '../validators/otherHolidaysDuringSchoolBreak';
import { otherHolidaysOutsideSchoolSeason } from '../validators/otherHolidaysOutsideSchoolSeason';

const ValidatorLookup = {
  required: Validators.required,
  minDate,
  minDateWarning,
  maxDate,
  maxDateWarning,
  holidaysOverlap,
  otherHolidaysDuringSchoolBreak,
  otherHolidaysOutsideSchoolSeason,
};

const ControlTypeLookup = {
  formControl: createFormControl,
  formArray: createFormArray
};

export function createForm(formContract: any) {
  const form = {};
  for (const key in formContract) {
    const obj = formContract[key];
    const createControl = ControlTypeLookup[obj.type];
    if (createControl) {
      form[key] = createControl(obj);
    }
  }
  const validators = createValidators(formContract);
  return new FormGroup(form, { validators, updateOn: 'blur' });
}

function createFormControl(obj: any) {
  const validators = createValidators(obj);
  return new FormControl(obj.value, validators);
}

function createFormArray(obj: any) {
  const controls = obj.controls.map(x => createFormControl(x));
  const validators = createValidators(obj);
  return new FormArray(controls, validators);
}

function createValidators(obj: any) {
  const validators: ValidatorFn[] = [];
  for (const key in obj.validators) {
    const props = obj.validators[key];
    const validatorFn = ValidatorLookup[key];
    if (validatorFn) {
      validators.push(props ? validatorFn(props) : validatorFn);
    }
  }
  return validators;
}
