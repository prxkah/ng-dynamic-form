import { omit, isEmpty, has } from 'lodash';

export const setWarning = (control: any, newWarning: any, key: string) => {
  const oldWarnings = control.warnings;

  if (newWarning) {
    const updatedWarnings = { ...oldWarnings, [key]: { ...newWarning } };
    control.warnings = updatedWarnings;
  }

  if (!newWarning && has(oldWarnings, key)) {
    const updatedWarnings = omit(oldWarnings, [key]);
    control.warnings = isEmpty(updatedWarnings) ? undefined : updatedWarnings;
  }
};
