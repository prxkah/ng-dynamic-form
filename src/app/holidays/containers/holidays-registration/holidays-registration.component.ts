import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, AbstractControl, FormArray, FormBuilder, Validators } from '@angular/forms';
import { minDate, minDateWarning } from '../../validators/minDate';
import { otherHolidaysDuringSchoolBreak } from '../../validators/otherHolidaysDuringSchoolBreak';
import { otherHolidaysOutsideSchoolSeason } from '../../validators/otherHolidaysOutsideSchoolSeason';
import { maxDateWarning } from '../../validators/maxDate';

interface CustomAbstractControl extends AbstractControl {
  warnings: any;
}

@Component({
  selector: 'app-holidays-registration',
  templateUrl: './holidays-registration.component.html'
})
export class HolidaysRegistrationComponent implements OnInit {
  get warnings() {
    const warnings = {};

    for (const key in this.form.value) {
      const control = this.form.controls[key] as CustomAbstractControl;
      if (control.warnings) {
        warnings[key] = control.warnings;
      }
    }

    return Object.keys(warnings).length === 0 ? null : warnings;
  }

  @Input() formContract: any;

  form: FormGroup;

  get otherHolidays() {
    return this.form.get('otherHolidays') as FormArray;
  }

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      workyearStart: ['2019-08-13', Validators.required],
      autumnSeasonEnd: ['2019-12-20', Validators.required],
      springSeasonStart: ['2020-01-09', Validators.required],
      workyearEnd: ['2020-06-13', Validators.required],
      sportBreakStart: ['2020-02-25', [Validators.required, minDateWarning('2020-02-26')]],
      sportBreakEnd: ['2020-02-28', [Validators.required, maxDateWarning('2020-02-26')]],
      easterBreakStart: ['2020-04-15', Validators.required],
      easterBreakEnd: ['2020-04-17', Validators.required],
      otherHolidays: fb.array(['', '', '', '', '', '', '', ''])
    });

    this.form.setValidators([otherHolidaysDuringSchoolBreak, otherHolidaysOutsideSchoolSeason]);
  }

  ngOnInit() {}

  update() {
    const data = {
      workyearStart: '',
      autumnSeasonEnd: '',
      springSeasonStart: '',
      workyearEnd: '',
      sportBreakStart: '',
      sportBreakEnd: '',
      easterBreakStart: '',
      easterBreakEnd: '',
      otherHolidays: ['asd', 'kok', 'asdad']
    };

    this.form.patchValue(data);
    this.form.controls.otherHolidays.patchValue(data.otherHolidays);
  }
}
