import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HolidaysStore } from './holidays.store';
import { HolidaysQuery } from './holidays.query';

@Injectable({ providedIn: 'root' })
export class HolidaysService {
  constructor(private store: HolidaysStore, private query: HolidaysQuery, private http: HttpClient) {}

  getHolidaysForm() {
    return this.http.get('/assets/holidays-form.json');
  }
}
