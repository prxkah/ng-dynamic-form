import { Injectable } from '@angular/core';
import { StoreConfig, Store } from '@datorama/akita';
import { HolidayDetails } from './holidays.model';

export interface State {
  holidays: HolidayDetails
}

const initialState: State = {
  holidays: null
};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'holidays' })
export class HolidaysStore extends Store<State> {
  constructor() {
    super(initialState);
  }
}
