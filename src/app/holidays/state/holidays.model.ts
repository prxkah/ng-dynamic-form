export interface HolidayDetails {
  workyearStart: string;
  autumnSeasonEnd: string;
  springSeasonStart: string;
  workyearEnd: string;
}
