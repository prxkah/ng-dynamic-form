import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { State, HolidaysStore } from './holidays.store';

@Injectable({ providedIn: 'root' })
export class HolidaysQuery extends Query<State> {
  constructor(protected store: HolidaysStore) {
    super(store);
  }
}
