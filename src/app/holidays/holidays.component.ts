import { Component, OnInit } from '@angular/core';
import { HolidaysService } from './state/holidays.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-holidays',
  template: `

  <button (click)="toggleFormVisibility()">Visa/Dölj</button>
  <ng-container *ngIf="isFormVisible">
    <app-holidays-registration
      *ngIf="holidaysForm$ | async as holidayForm"
      [formContract]="holidayForm">
    </app-holidays-registration>
  </ng-container>
  `
})
export class HolidaysComponent implements OnInit {
  isFormVisible = false;
  holidaysForm$: Observable<any>;

  constructor(private holidaysService: HolidaysService) {
    this.holidaysForm$ = this.holidaysService.getHolidaysForm();
  }

  ngOnInit() {}

  toggleFormVisibility() {
    this.isFormVisible = !this.isFormVisible;
  }
}
