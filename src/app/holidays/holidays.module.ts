import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { HolidaysComponent } from './holidays.component';
import { HolidaysRegistrationComponent } from './containers/holidays-registration/holidays-registration.component';
import { HolidayFieldGroupComponent } from './components/holiday-field-group/holiday-field-group.component';
import { SortByPipe } from './pipes/sorter.pipe';

const routes: Routes = [{ path: '', component: HolidaysComponent }];

@NgModule({
  declarations: [HolidaysComponent, HolidaysRegistrationComponent, SortByPipe, HolidayFieldGroupComponent],
  imports: [CommonModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [],
  providers: []
})
export class HolidaysModule {}
